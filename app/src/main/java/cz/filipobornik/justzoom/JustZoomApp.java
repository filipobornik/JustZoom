package cz.filipobornik.justzoom;

import android.app.Activity;
import android.app.Application;
import com.crashlytics.android.Crashlytics;
import cz.filipobornik.justzoom.di.component.DaggerAppComponent;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

import io.fabric.sdk.android.Fabric;
import javax.inject.Inject;

/**
 * Application
 */
public class JustZoomApp extends Application implements HasActivityInjector {

    /**
     * DispatchingAndroidInjector for dagger
     */
    @Inject DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        DaggerAppComponent.builder().create(this).inject(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }
}
