package cz.filipobornik.justzoom.data.remote;

import cz.filipobornik.justzoom.data.model.OCRApiResponse;
import io.reactivex.Flowable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.*;

public interface OCRSpaceService {

    @POST("parse/image")
    @Multipart
    Flowable<OCRApiResponse> getTextFromImage(@Part("apikey") String apiKey, @Part MultipartBody.Part photo, @Part("language")RequestBody language);

}
