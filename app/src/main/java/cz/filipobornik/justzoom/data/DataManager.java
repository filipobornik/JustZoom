package cz.filipobornik.justzoom.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import com.google.android.gms.common.util.ArrayUtils;
import cz.filipobornik.justzoom.R;
import cz.filipobornik.justzoom.data.model.ItemImageText;
import cz.filipobornik.justzoom.data.remote.OCRSpaceService;
import cz.filipobornik.justzoom.di.ApplicationContext;
import cz.filipobornik.justzoom.utils.Constants;
import cz.filipobornik.justzoom.utils.SchedulersProvider;
import cz.filipobornik.justzoom.utils.Utils;
import io.reactivex.Flowable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import javax.inject.Inject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Array;
import java.text.SimpleDateFormat;
import java.util.*;

public final class DataManager {

    private OCRSpaceService ocrSpaceService;

    private SchedulersProvider schedulersProvider;

    private SharedPreferences sharedPreferences;

    private @ApplicationContext Context context;

    private Resources resourceManager;

    @Inject
    public DataManager(OCRSpaceService ocrSpaceService, SchedulersProvider schedulersProvider, SharedPreferences sharedPreferences,
                       @ApplicationContext Context context, Resources resourceManager) {
        this.ocrSpaceService = ocrSpaceService;
        this.schedulersProvider = schedulersProvider;
        this.sharedPreferences = sharedPreferences;
        this.context = context;
        this.resourceManager = resourceManager;
    }

    public Flowable<String> getTextFromImage(File imageFile) {
        MultipartBody.Part body = null;
        if (imageFile != null) {
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), imageFile);
            body = MultipartBody.Part.createFormData("photo", imageFile.getName(), reqFile);
        }

        String language = sharedPreferences.getString("pref_speak_language", "en");

        if (language.equals("cs")) language = "cze";
        if (language.equals("en")) language = "eng";

        RequestBody description = RequestBody.create(MediaType.parse("multipart/form-data"), language);
        return ocrSpaceService.getTextFromImage(Constants.OCR_SPACE_API_KEY,body, description)
                .subscribeOn(schedulersProvider.io())
                .flatMap(ocrApiResponse -> {
                    String text = "no text found";
                    if (ocrApiResponse.getParsedResults() != null) {
                        text = ocrApiResponse.getParsedResults().get(0).getParsedText();
                    }
                    return Flowable.just(text);
                })
                .observeOn(schedulersProvider.ui());
    }

    /**
     * Save text from image associated with the image by name (TXT_{image file name}.txt)
     * @param imageUri Uri of image that is associated with text
     * @param text Text to save
     */
    public Uri saveTextAssociatedWithImage(Uri imageUri, String text) {
        File imageFile = new File(imageUri.getPath());

        File mediaStorageDir = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), resourceManager.getString(R.string.app_name));

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        try {
            File destFile = new File(mediaStorageDir, "TXT_" + imageFile.getName().replace(".jpg", "") + ".txt");
            FileOutputStream outputStream = new FileOutputStream(destFile);
            outputStream.write(text.getBytes());
            outputStream.close();
            return Uri.fromFile(destFile);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Save image into storage and return Uri for the image
     */
    public Uri saveImageIntoStorage(Bitmap image) {
        if (image == null) {
            return null;
        }

        File mediaStorageDir = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), resourceManager.getString(R.string.app_name));

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        try {
            File destFile = File.createTempFile("IMG_" + timeStamp, ".jpg", mediaStorageDir);
            FileOutputStream outputStream = new FileOutputStream(destFile);
            image.compress(Bitmap.CompressFormat.JPEG, 60, outputStream);
            outputStream.close();
            return Uri.fromFile(destFile);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Find all images and if available text files associated with them
     * @return array list of images with associated text if available
     */
    public ArrayList<ItemImageText> getItemsImageText() {
        File mediaStorageDir = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), resourceManager.getString(R.string.app_name));
        if (!mediaStorageDir.exists()) {
            return new ArrayList<>();
        }

        ArrayList<ItemImageText> items = new ArrayList<>();

        List<File> imgFiles = Arrays.asList(mediaStorageDir.listFiles(((dir, name) -> name.toLowerCase().endsWith(".jpg"))));
        List<File> txtFiles = Arrays.asList(mediaStorageDir.listFiles(((dir, name) -> name.toLowerCase().endsWith(".txt"))));
        List<String> txtFilesNames = new ArrayList<>();
        for (File txtFile : txtFiles) {
            txtFilesNames.add(Utils.getImageFileNameFromTxtFileName(txtFile.getName()));
        }
        Collections.reverse(imgFiles);
        for (File file : imgFiles) {
            String date = Utils.getDateFromImageFileName(file.getName());
            boolean isTxtSaved = false;
            Uri txtUri = null;
            String name = file.getName();

            for (String fileName : txtFilesNames) {
                if (name.contains(fileName)) {
                    isTxtSaved = true;
                    txtUri = Uri.fromFile(txtFiles.get(txtFilesNames.indexOf(name)));
                }
            }

            ItemImageText itemImageText = new ItemImageText(name, Uri.fromFile(file), date, isTxtSaved, txtUri);
            items.add(itemImageText);
        }
        return items;
    }

    /**
     * Delete TXT file associated with specific image
     * @param takenImageUri
     */
    public void deleteTxtFileForImage(Uri takenImageUri) {
        if (takenImageUri == null) return;
        File deleteTxtFile = new File(takenImageUri.getEncodedPath().replaceAll(resourceManager.getString(R.string.app_name) + ".*",
                resourceManager.getString(R.string.app_name) + "/") +
                "TXT_" + new File(takenImageUri.getEncodedPath()).getName().replace(".jpg", "") + ".txt");
        if (deleteTxtFile.exists()) {
            deleteTxtFile.delete();
        }
    }
}
