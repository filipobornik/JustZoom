package cz.filipobornik.justzoom.data.model;

import android.net.Uri;
import android.support.annotation.NonNull;

public class ItemImageText implements Comparable<ItemImageText> {

    private String title;

    private Uri imageUri;

    private String captureDate;

    private boolean isTextSaved;

    private Uri textUri;

    public ItemImageText(String name, Uri imageUri, String releaseDate, boolean isTextSaved, Uri textUri) {
        this.title = name;
        this.imageUri = imageUri;
        this.captureDate = releaseDate;
        this.isTextSaved = isTextSaved;
        this.textUri = textUri;
    }

    public String getTitle() {
        return title;
    }

    public Uri getImageUri() {
        return imageUri;
    }

    public String getCaptureDate() {
        return captureDate;
    }

    public boolean isTextSaved() {
        return isTextSaved;
    }

    public Uri getTextUri() {
        return textUri;
    }

    @Override
    public int compareTo(@NonNull ItemImageText itemImageText) {
        return title.compareTo(itemImageText.title);
    }
}
