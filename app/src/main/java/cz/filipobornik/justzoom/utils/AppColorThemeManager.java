package cz.filipobornik.justzoom.utils;

import android.content.SharedPreferences;
import android.support.annotation.ColorRes;
import android.support.annotation.IdRes;
import android.support.annotation.StyleRes;
import android.util.Log;
import cz.filipobornik.justzoom.R;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AppColorThemeManager {

    public static String PREF_APP_COLOR_THEME = "pref_color_theme";

    @Inject SharedPreferences sharedPreferences;

    @StyleRes private int appColorTheme;
    @StyleRes private int appColorThemeNoActionBar;

    @Inject
    public AppColorThemeManager(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
        updateColorTheme();
    }

    public void updateColorTheme() {
        int index = sharedPreferences.getInt(PREF_APP_COLOR_THEME, 0);
        switch (index) {
            case 0:
                appColorTheme = R.style.AppThemeActionBar;
                appColorThemeNoActionBar = R.style.AppTheme;
                break;
            case 1:
                appColorTheme = R.style.YellowAppThemeActionBar;
                appColorThemeNoActionBar = R.style.YellowAppTheme;
                break;
            case 2:
                appColorTheme = R.style.BlueAppThemeActionBar;
                appColorThemeNoActionBar = R.style.BlueAppTheme;
                break;
        }
    }

    public @StyleRes
    int getAppColorTheme() {
        Log.d("colorTheme", "Returned: " + appColorTheme);
        return appColorTheme;
    }

    public @StyleRes
    int getAppColorThemeNoActionBar() {
        return appColorThemeNoActionBar;
    }

    public @ColorRes int getColorForFAB() {
        int index = sharedPreferences.getInt(PREF_APP_COLOR_THEME, 0);
        switch (index) {
            case 0:
                return R.color.colorPrimary;
            case 1:
                return R.color.material_amber500;
            case 2:
                return R.color.material_blue500;
        }
        return R.color.colorPrimary;
    }

}
