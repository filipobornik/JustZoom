package cz.filipobornik.justzoom.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utils {

    /**
     * Get date from image file name in "dd. mm. yyyy" format
     * @param imageFileName file name of the image
     * @return text representing date in "dd. mm. yyyy" format or empty string
     */
    public static String getDateFromImageFileName(String imageFileName) {
        String[] parts = imageFileName.split("_");
        DateFormat inputDateFormat = new SimpleDateFormat("yyyymmdd", Locale.getDefault());
        DateFormat outputDateFormat = new SimpleDateFormat("dd. mm. yyyy", Locale.getDefault());
        try {
            Date date;
            if (parts[1].equals("EDIT")) {
                date = inputDateFormat.parse(parts[2]);
            } else {
                date = inputDateFormat.parse(parts[1]);
            }
            return outputDateFormat.format(date);
        } catch (ParseException e) {
            return "";
        }
    }

    /**
     * Get image file name from txt file name
     * @param txtFileName txt file name
     * @return text of image file associated with txt file
     */
    public static String getImageFileNameFromTxtFileName(String txtFileName) {
        String temp = txtFileName;
        temp = temp.replace("TXT_", "");
        temp = temp.replace(".txt", ".jpg");
        if (temp.contains("EDIT")) {
            temp = temp.replace("_EDIT", "");
        }
        return temp.split("#")[0];
    }
}
