package cz.filipobornik.justzoom.utils;

/**
 * Constants
 */
public class Constants {

    public static String OCR_SPACE_API_KEY = "2dea65789c88957";
    public static String OCR_SPACE_BASE_URL = "https://api.ocr.space/";

    public static String PREF_SPEAK_LANGUAGE = "pref_speak_language";
    public static final String PREF_FIRST_START = "pref_first_start";

}
