package cz.filipobornik.justzoom.ui.cameraActivity;

import android.Manifest;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import com.wonderkiln.camerakit.*;
import cz.filipobornik.justzoom.R;
import cz.filipobornik.justzoom.base.BaseActivity;
import cz.filipobornik.justzoom.ui.imageDetailActivity.ImageDetailActivity;
import cz.filipobornik.justzoom.ui.settingsActivity.SettingsActivity;
import cz.filipobornik.justzoom.ui.takenImagesGalleryActivity.TakenImagesGalleryActivity;
import cz.filipobornik.justzoom.utils.AppColorThemeManager;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import javax.inject.Inject;
import java.io.IOException;

/**
 * Shows camera preview and enables user to capture image
 */
public class CameraActivity extends BaseActivity
        implements CameraActivityContract.View {

    /**
     * Request camera permission
     */
    private static final int REQUEST_PERMISSION_CAMERA = 100;

    /**
     * Request permission for reading external storage to get user's selected image from gallery
     */
    private static final int REQUEST_READ_EXTERNAL_STORAGE = 101;

    private static final int REQUEST_CONTENT_CODE = 200;

    /**
     * Log tag for debugging
     */
    private static final String LOG_TAG = CameraActivity.class.getSimpleName();

    /**
     * Presenter for CameraActivity
     */
    @Inject CameraActivityContract.Presenter presenter;

    /**
     * Resource manager that gives access to resources
     */
    @Inject Resources resourceManager;

    /**
     * AppColorThemeManager manage app colors
     */
    @Inject AppColorThemeManager colorThemeManager;


    @BindView(R.id.fabFlash) FloatingActionButton fabFlash;
    @BindView(R.id.fabCapture) FloatingActionButton fabCapture;
    @BindView(R.id.fabGallery) FloatingActionButton fabGallery;
    @BindView(R.id.camera) CameraView cameraView;
    @BindView(R.id.loadingBar) ProgressBar loadingBar;

    /**
     * Start image DetailImageActivity, add image as extra to the intent
     */
    @Override
    public void startDetailImageActivity(Uri imageUri) {
        Intent intent = new Intent(this, ImageDetailActivity.class);
        intent.putExtra(ImageDetailActivity.INTENT_EXTRA_IMG_PATH, imageUri.getPath());
        Log.d(LOG_TAG, "Path: " + imageUri.getEncodedPath());
        startActivity(intent);
    }

    /**
     * Start SettingsActivity
     */
    @Override
    public void startSettingsActivity() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    /**
     * Start TakenImagesGalleryActivity
     */
    @Override
    public void startTakenImagesGalleryActivity() {
        Intent intent = new Intent(this, TakenImagesGalleryActivity.class);
        startActivity(intent);
    }

    /**
     * Start Action get content activity to let user select image he want to use
     */
    @Override
    public void startPickImageActivity() {
        String[] mimeTypes = {"image/jpeg", "image/png"};
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT)
                .setType("image/*")
                .putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(intent, REQUEST_CONTENT_CODE);
    }

    /**
     * Show loading progress bar
     */
    @Override
    public void showLoading() {
        loadingBar.setVisibility(View.VISIBLE);
    }

    /**
     * Hide loading progress bar
     */
    @Override
    public void hideLoading() {
        loadingBar.setVisibility(View.GONE);
    }

    /**
     * Handle on permission granted
     */
    @AfterPermissionGranted(REQUEST_PERMISSION_CAMERA)
    private void checkCameraPermission() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA)) {
            startCamera();
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.camera_request_rational), REQUEST_PERMISSION_CAMERA, Manifest.permission.CAMERA);
        }
    }

    /**
     * Check if you have permission for camera
     * If not - request it
     * If yes - start camera
     */
    private void openCamera() {
        checkCameraPermission();
    }

    /**
     * Setup camera and start it
     */
    private void startCamera() {
        cameraView.setFocus(CameraKit.Constants.FOCUS_TAP_WITH_MARKER);
        cameraView.setMethod(CameraKit.Constants.METHOD_STILL);
        cameraView.start();
    }

    @Override
    public void stopCamera() {
        cameraView.stop();
    }

    /**
     * Notifies presenter about fab flash click
     *
     * @param view view that has been clicked
     */
    @OnClick(R.id.fabFlash)
    public void onFabFlashClicked(View view) {
        presenter.onFabFlashClicked();
    }

    /**
     * Capture image, show loading and notify presenter about image captured
     *
     * @param view view that has been clicked
     */
    @OnClick(R.id.fabCapture)
    public void onFabCaptureClicked(View view) {
        cameraView.addCameraKitListener(new CameraKitEventListener() {
            @Override
            public void onEvent(CameraKitEvent cameraKitEvent) {

            }

            @Override
            public void onError(CameraKitError cameraKitError) {

            }

            @Override
            public void onImage(CameraKitImage cameraKitImage) {
                presenter.onFabCaptureClicked(cameraKitImage);
            }

            @Override
            public void onVideo(CameraKitVideo cameraKitVideo) {

            }
        });
        cameraView.captureImage();
        cameraView.setVisibility(View.GONE);
        showLoading();
    }

    /**
     * Notifies presenter about fab gallery clicked
     *
     * @param view view that has been clicked
     */
    @OnClick(R.id.fabGallery)
    public void onFabGalleryClicked(View view) {
        presenter.onFabGalleryClicked();
    }

    /**
     * Notifies presenter about btn settings click
     *
     * @param view view that has been clicked
     */
    @OnClick(R.id.btnSettings)
    public void onBtnSettingsClicked(View view) {
        presenter.onBtnSettingsClicked();
    }

    /**
     * Notifies presenter about btn saved images gallery click
     *
     * @param view view that has been clicked
     */
    @OnClick(R.id.btnSavedImagesGallery)
    public void onBtnSavedImagesGallery(View view) {
        presenter.onBtnSavedImagesGallery();
    }

    /**
     * Handles image selection
     */
    @Override
    public void onImagePicked(Uri imageUri) {
        if (!EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            EasyPermissions.requestPermissions(this, getString(R.string.need_permission_read_external_storage), REQUEST_READ_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
            return;
        }
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
            presenter.onImgSelected(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    @OnTouch(R.id.camera)
    public boolean onTouchEvent(MotionEvent event) {

        return super.onTouchEvent(event);
    }

    /**
     * Turn flash on and change flash fab icon
     */
    @Override
    public void turnFlashOn() {
        fabFlash.setImageResource(R.drawable.ic_flash_off_white_24dp);
        cameraView.setFlash(CameraKit.Constants.FLASH_TORCH);
    }

    /**
     * Turn flash off and change flash fab icon
     */
    @Override
    public void turnFlashOff() {
        fabFlash.setImageResource(R.drawable.ic_flash_on_white_24dp);
        cameraView.setFlash(CameraKit.Constants.FLASH_OFF);
    }

    /**
     * Set fab buttons colors from color theme that user picked in settings
     */
    private void setColorsToFabButtons() {
        fabFlash.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, colorThemeManager.getColorForFAB())));
        fabCapture.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, colorThemeManager.getColorForFAB())));
        fabGallery.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, colorThemeManager.getColorForFAB())));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CONTENT_CODE) {
            if (data != null) {
                onImagePicked(data.getData());
            }
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(colorThemeManager.getAppColorThemeNoActionBar());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_camera);
        ButterKnife.bind(this);
        presenter.attachView(this);
    }

    @Override
    protected void onStart() {
        setColorsToFabButtons();
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        openCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        cameraView.stop();
        cameraView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }
}
