package cz.filipobornik.justzoom.ui.imageDetailActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import cz.filipobornik.justzoom.R;
import cz.filipobornik.justzoom.base.BaseActivity;
import cz.filipobornik.justzoom.ui.cameraActivity.CameraActivity;
import cz.filipobornik.justzoom.ui.editImageActivity.EditImageActivity;

import javax.inject.Inject;
import java.io.*;

/**
 * Show detail of the captured image, let user to discard or save the image
 */
public class ImageDetailActivity extends BaseActivity implements ImageDetailActivityContract.View, BottomNavigationView.OnNavigationItemSelectedListener {

    /**
     * Intent extra image path key
     */
    public static String INTENT_EXTRA_IMG_PATH = "ImageDetailActivity.intentExtraImagePath";

    /**
     * Intent extra image path key
     */
    public static String INTENT_EXTRA_TXT_PATH = "ImageDetailActivity.intentExtraTextPath";

    /**
     * Presenter for ImageDetailActivity
     */
    @Inject ImageDetailActivityContract.Presenter presenter;

    /**
     * Resource manager that gives access to resources
     */
    @Inject Resources resourceManager;

    @BindView(R.id.imageView) ImageView imageView;
    @BindView(R.id.bottomNavigation) BottomNavigationView bottomNavigationView;
    @BindView(R.id.textLayout) ConstraintLayout textLayout;
    @BindView(R.id.text) TextView textView;
    @BindView(R.id.imgArrow) ImageView imageArrow;


    /**
     * Uri to captured image
     */
    private Uri takenImageUri;

    /**
     * Indicates if text view is shown
     */
    private boolean isTextViewShown = false;

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    /**
     * Start CameraActivity
     */
    @Override
    public void startCameraActivity() {
        Intent intent = new Intent(this, CameraActivity.class);
        startActivity(intent);
    }

    /**
     * Start EditImageActivity to let user edit the image
     */
    @Override
    public void startEditImageActivity() {
        Intent intent = new Intent(this, EditImageActivity.class);
        intent.putExtra(INTENT_EXTRA_IMG_PATH, takenImageUri.getPath());
        startActivity(intent);
    }

    private void shareImageWithText() {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        if (getIntent().hasExtra(INTENT_EXTRA_TXT_PATH)) {
            shareIntent.putExtra(Intent.EXTRA_TEXT, getImageText(getImageText(getIntent().getStringExtra(INTENT_EXTRA_TXT_PATH))));
        }
        Uri uri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".cz.filipobornik.justzoom.data.GenericFileProvider", new File(takenImageUri.getEncodedPath()));
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.setType("*/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(shareIntent, "Share images..."));
    }

    /**
     * Delete taken image
     */
    @Override
    public void deleteImage() {
        presenter.deleteImageTxt(takenImageUri);
        File deleteFile = new File(takenImageUri.getPath());
        if (deleteFile.exists()) {
            if (deleteFile.delete()) {
                showMessage(R.string.image_deleted);
            } else {
                showError(R.string.image_not_deleted);
            }
        }
    }

    private void showText(String path) {
        textView.setText(getImageText(path));
        imageArrow.setImageDrawable(resourceManager.getDrawable(R.drawable.ic_keyboard_arrow_down_black_24dp));
        isTextViewShown = true;
    }

    private String getImageText(String path) {
        File file = new File(path);
        StringBuffer text = new StringBuffer();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text.toString();
    }

    private void hideText() {
        textView.setText(resourceManager.getString(R.string.show_text));
        imageArrow.setImageDrawable(resourceManager.getDrawable(R.drawable.ic_keyboard_arrow_up));
        isTextViewShown = false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_not_save:
                deleteImage();
                finish();
                break;
            case R.id.action_edit:
                startEditImageActivity();
                break;
            case R.id.action_save:
                showMessage(R.string.image_saved);
                startCameraActivity();
                finish();
                break;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() ==  android.R.id.home) {
            finish();
            return false;
        }
        if (item.getItemId() == R.id.action_share) {
            shareImageWithText();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.image_detail_toolbar, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_detail);
        ButterKnife.bind(this);
        presenter.attachView(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.image_detail);
        }

        if (getIntent().hasExtra(INTENT_EXTRA_IMG_PATH)) {
            takenImageUri = Uri.fromFile(new File(getIntent().getStringExtra(INTENT_EXTRA_IMG_PATH)));
        }
        if (getIntent().hasExtra(INTENT_EXTRA_TXT_PATH)) {
            textLayout.setVisibility(View.VISIBLE);
            imageArrow.setOnClickListener(view -> {
                if (isTextViewShown) {
                    hideText();
                } else {
                    showText(getIntent().getStringExtra(INTENT_EXTRA_TXT_PATH));
                }
            });
        }
        bottomNavigationView.setSelectedItemId(R.id.action_edit);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        RequestOptions requestOptions = RequestOptions
                .skipMemoryCacheOf(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE);
        Glide.with(this)
                .load(takenImageUri)
                .apply(requestOptions)
                .into(imageView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

}
