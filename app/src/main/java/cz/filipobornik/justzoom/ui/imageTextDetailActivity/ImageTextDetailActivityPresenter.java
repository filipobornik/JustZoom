package cz.filipobornik.justzoom.ui.imageTextDetailActivity;

import cz.filipobornik.justzoom.base.BasePresenter;
import cz.filipobornik.justzoom.di.PerActivity;

import javax.inject.Inject;

@PerActivity
public class ImageTextDetailActivityPresenter extends BasePresenter<ImageTextDetailActivityContract.View>
        implements ImageTextDetailActivityContract.Presenter {

    @Inject
    public ImageTextDetailActivityPresenter() {
    }

}

