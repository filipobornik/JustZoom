package cz.filipobornik.justzoom.ui.takenImagesGalleryActivity;

import android.support.v7.app.AppCompatActivity;
import cz.filipobornik.justzoom.base.BaseActivityModule;
import cz.filipobornik.justzoom.di.PerActivity;
import dagger.Binds;
import dagger.Module;

@Module(includes = BaseActivityModule.class)
public abstract class TakenImagesGalleryActivityModule {

    @Binds
    @PerActivity
    abstract AppCompatActivity provideActivity(TakenImagesGalleryActivity cameraActivity);

    @Binds
    @PerActivity
    abstract TakenImagesGalleryActivityContract.View provideMainActivityAsView(TakenImagesGalleryActivity cameraActivity);

    @Binds
    @PerActivity
    abstract TakenImagesGalleryActivityContract.Presenter providePresenter(TakenImagesGalleryPresenter presenter);

}
