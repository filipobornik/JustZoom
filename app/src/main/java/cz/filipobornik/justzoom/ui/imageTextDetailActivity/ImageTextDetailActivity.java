package cz.filipobornik.justzoom.ui.imageTextDetailActivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import cz.filipobornik.justzoom.R;
import cz.filipobornik.justzoom.base.BaseActivity;

public class ImageTextDetailActivity extends BaseActivity implements ImageTextDetailActivityContract.View {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_text_detail);
    }
}
