package cz.filipobornik.justzoom.ui.mainActivity;

import cz.filipobornik.justzoom.base.BasePresenter;
import cz.filipobornik.justzoom.di.PerActivity;

import javax.inject.Inject;

@PerActivity
public class MainActivityPresenter extends BasePresenter<MainActivityContract.View> implements MainActivityContract.Presenter {

    @Inject
    public MainActivityPresenter() {
    }

}
