package cz.filipobornik.justzoom.ui.settingsActivity;

import android.annotation.TargetApi;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.MenuItem;
import cz.filipobornik.justzoom.R;
import cz.filipobornik.justzoom.di.PerActivity;
import cz.filipobornik.justzoom.ui.aboutApp.AboutAppFragment;
import cz.filipobornik.justzoom.ui.mainActivity.MainActivity;
import cz.filipobornik.justzoom.utils.AppColorThemeManager;
import dagger.android.AndroidInjection;

import javax.inject.Inject;

/**
 * Settings activity enables user to change settings, color themes, language, etc
 */
@PerActivity
public class SettingsActivity extends AppCompatPreferenceActivity {

    @Inject AppColorThemeManager colorThemeManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        colorThemeManager.updateColorTheme();
        setTheme(colorThemeManager.getAppColorTheme());
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new GeneralPreferenceFragment())
                .commit();
        setupActionBar();
    }

    /**
     * Enable home as up navigation if action bar is available
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * This fragment shows general preferences as language, color theme, etc
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class GeneralPreferenceFragment extends PreferenceFragment implements Preference.OnPreferenceChangeListener {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);
            setHasOptionsMenu(true);

            Preference colorThemePreference = findPreference("pref_color_theme");
            colorThemePreference.setOnPreferenceChangeListener(this);
            Preference speakLanguagePreference = findPreference("pref_speak_language");
            speakLanguagePreference.setOnPreferenceChangeListener(this);

            Preference aboutAppPref = findPreference("aboutApp");
            aboutAppPref.setOnPreferenceClickListener(preference -> {
                getActivity().getFragmentManager().beginTransaction()
                        .replace(android.R.id.content, new AboutAppFragment())
                        .addToBackStack("aboutApp")
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                return true;
            });
        }

        /**
         * Handle on options item selected
         *
         * @param item item that has been clicked
         * @return boolean
         */
        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), MainActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        /**
         * Handle preference change
         *
         * @param preference preference that has been changed
         * @param newValue   value set for preference
         * @return true
         */
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);

            if (preference instanceof ListPreference) {
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(newValue.toString());

                // Set the summary to reflect the new value.
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);

                if (preference.getKey().equals("pref_color_theme")) {
                    sharedPreferences.edit()
                            .putInt(AppColorThemeManager.PREF_APP_COLOR_THEME, index)
                            .apply();
                    getActivity().recreate();
                } else if (preference.getKey().equals("pref_speak_language")) {
                    Log.d("langNew", "" + listPreference.getEntryValues()[index]);
                    sharedPreferences.edit()
                            .putString("pref_speak_language", listPreference.getEntryValues()[index].toString())
                            .apply();
                }
            }
            return true;
        }
    }
}
