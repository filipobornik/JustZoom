package cz.filipobornik.justzoom.ui.imageTextDetailActivity;

import cz.filipobornik.justzoom.base.BaseMVPPresenter;
import cz.filipobornik.justzoom.base.BaseMVPView;

public interface ImageTextDetailActivityContract {

    interface View extends BaseMVPView {

    }

    interface Presenter extends BaseMVPPresenter<View> {

    }

}
