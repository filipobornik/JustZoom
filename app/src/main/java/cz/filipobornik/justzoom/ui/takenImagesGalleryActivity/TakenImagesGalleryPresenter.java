package cz.filipobornik.justzoom.ui.takenImagesGalleryActivity;

import cz.filipobornik.justzoom.base.BasePresenter;
import cz.filipobornik.justzoom.data.DataManager;
import cz.filipobornik.justzoom.data.model.ItemImageText;
import cz.filipobornik.justzoom.di.PerActivity;

import javax.inject.Inject;
import java.util.ArrayList;

@PerActivity
public class TakenImagesGalleryPresenter extends BasePresenter<TakenImagesGalleryActivityContract.View>
        implements TakenImagesGalleryActivityContract.Presenter {

    private DataManager dataManager;

    @Inject
    TakenImagesGalleryPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void loadImageTextItems() {
        view.showLoading();
        ArrayList<ItemImageText> imageTextItems = dataManager.getItemsImageText();
        view.updateItems(imageTextItems);
        view.hideLoading();
        if (imageTextItems.isEmpty()) {
            view.showEmptyView();
        } else {
            view.hideEmptyView();
        }
    }
}
