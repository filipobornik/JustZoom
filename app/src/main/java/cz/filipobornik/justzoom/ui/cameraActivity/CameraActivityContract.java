package cz.filipobornik.justzoom.ui.cameraActivity;

import android.graphics.Bitmap;
import android.net.Uri;
import com.wonderkiln.camerakit.CameraKitImage;
import cz.filipobornik.justzoom.base.BaseMVPPresenter;
import cz.filipobornik.justzoom.base.BaseMVPView;

import java.io.File;

public abstract class CameraActivityContract {

    interface View extends BaseMVPView {

        void startDetailImageActivity(Uri imageUri);

        void startSettingsActivity();

        void startPickImageActivity();

        void startTakenImagesGalleryActivity();

        void stopCamera();

        void showLoading();

        void hideLoading();

        void turnFlashOn();

        void turnFlashOff();

        void onImagePicked(Uri uri);

    }

    interface Presenter extends BaseMVPPresenter<View> {

        void onFabCaptureClicked(CameraKitImage cameraKitImage);

        void onFabFlashClicked();

        void onFabGalleryClicked();

        void onBtnSettingsClicked();

        void onBtnSavedImagesGallery();

        void onImgSelected(Bitmap bitmap);

    }

}
