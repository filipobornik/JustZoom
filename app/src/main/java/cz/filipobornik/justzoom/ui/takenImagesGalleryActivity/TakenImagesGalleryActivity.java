package cz.filipobornik.justzoom.ui.takenImagesGalleryActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import cz.filipobornik.justzoom.R;
import cz.filipobornik.justzoom.base.BaseActivity;
import cz.filipobornik.justzoom.base.listeners.OnItemClickListener;
import cz.filipobornik.justzoom.data.DataManager;
import cz.filipobornik.justzoom.data.model.ItemImageText;
import cz.filipobornik.justzoom.ui.imageDetailActivity.ImageDetailActivity;
import cz.filipobornik.justzoom.utils.Constants;

import javax.inject.Inject;
import java.util.ArrayList;

/**
 * Taken Image Gallery show all images that has been saved in this app
 */
public class TakenImagesGalleryActivity extends BaseActivity implements TakenImagesGalleryActivityContract.View, OnItemClickListener {

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.loadingBar) ProgressBar loadingBar;
    @BindView(R.id.emptyListLayout) LinearLayout emptyListLayout;

    @Inject TakenImageGalleryActivityAdapter adapter;
    @Inject DataManager dataManager;
    @Inject TakenImagesGalleryActivityContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taken_images_gallery);
        setTitle(R.string.taken_images_gallery);
        presenter.attachView(this);
        ButterKnife.bind(this);
        adapter.setOnClickListener(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.loadImageTextItems();
    }

    /**
     * Notifies view to updates items in the adapter
     *
     * @param items items to show
     */
    @Override
    public void updateItems(ArrayList<ItemImageText> items) {
        adapter.updateItems(items);
    }

    /**
     * Shows loading bar and hides recycler view
     */
    @Override
    public void showLoading() {
        recyclerView.setVisibility(View.GONE);
        loadingBar.setVisibility(View.VISIBLE);
    }

    /**
     * Hides loading bar and shows recycler view
     */
    @Override
    public void hideLoading() {
        loadingBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    /**
     * Shows empty view layout and hides recycler view
     */
    @Override
    public void showEmptyView() {
        recyclerView.setVisibility(View.GONE);
        emptyListLayout.setVisibility(View.VISIBLE);
    }

    /**
     * Hides empty view layout and shows recycler view
     */
    @Override
    public void hideEmptyView() {
        emptyListLayout.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClick(ItemImageText itemImageText) {
        Intent intent = new Intent(this, ImageDetailActivity.class);
        intent.putExtra(ImageDetailActivity.INTENT_EXTRA_IMG_PATH, itemImageText.getImageUri().getEncodedPath());
        if (itemImageText.getTextUri() != null) intent.putExtra(ImageDetailActivity.INTENT_EXTRA_TXT_PATH, itemImageText.getTextUri().getEncodedPath());
        startActivity(intent);
    }
}
