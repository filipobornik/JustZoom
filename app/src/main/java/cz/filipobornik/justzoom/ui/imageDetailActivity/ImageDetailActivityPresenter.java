package cz.filipobornik.justzoom.ui.imageDetailActivity;

import android.net.Uri;
import cz.filipobornik.justzoom.base.BasePresenter;
import cz.filipobornik.justzoom.data.DataManager;
import cz.filipobornik.justzoom.di.PerActivity;

import javax.inject.Inject;

/**
 * Presenter for ImageDetailActivity
 */
@PerActivity
public class ImageDetailActivityPresenter extends BasePresenter<ImageDetailActivityContract.View> implements ImageDetailActivityContract.Presenter {

    private DataManager dataManager;

    @Inject
    public ImageDetailActivityPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void deleteImageTxt(Uri takenImageUri) {
        dataManager.deleteTxtFileForImage(takenImageUri);
    }
}
