package cz.filipobornik.justzoom.ui.takenImagesGalleryActivity;

import cz.filipobornik.justzoom.base.BaseMVPPresenter;
import cz.filipobornik.justzoom.base.BaseMVPView;
import cz.filipobornik.justzoom.data.model.ItemImageText;

import java.util.ArrayList;

public class TakenImagesGalleryActivityContract {

    interface View extends BaseMVPView {

        void showLoading();

        void hideLoading();

        void showEmptyView();

        void hideEmptyView();

        void updateItems(ArrayList<ItemImageText> items);

    }

    interface Presenter extends BaseMVPPresenter<View> {

        void loadImageTextItems();

    }

}
