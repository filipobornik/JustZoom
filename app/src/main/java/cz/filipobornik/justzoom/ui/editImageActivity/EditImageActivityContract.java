package cz.filipobornik.justzoom.ui.editImageActivity;

import android.net.Uri;
import cz.filipobornik.justzoom.base.BaseMVPPresenter;
import cz.filipobornik.justzoom.base.BaseMVPView;

public abstract class EditImageActivityContract {

    interface View extends BaseMVPView {

        void startUCropActivity();

        void startOCRActivity();

        void refreshImageView();

        void saveImageChanges();

        void deleteEditedImage();

    }

    interface Presenter extends BaseMVPPresenter<View> {

        void saveImageChanges();

        void deleteImageTxt(Uri takenImageUri);

    }

}
