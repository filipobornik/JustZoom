package cz.filipobornik.justzoom.ui.aboutApp;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import cz.filipobornik.justzoom.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutAppFragment extends Fragment {


    public AboutAppFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_about_app, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView resourcesList = (TextView) getActivity().findViewById(R.id.resources_list);
        resourcesList.setMovementMethod(LinkMovementMethod.getInstance());
        TextView createdBy = (TextView) getActivity().findViewById(R.id.created_by);
        createdBy.setMovementMethod(LinkMovementMethod.getInstance());
    }


}
