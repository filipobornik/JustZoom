package cz.filipobornik.justzoom.ui.takenImagesGalleryActivity;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.BitmapEncoder;
import com.bumptech.glide.request.RequestOptions;
import cz.filipobornik.justzoom.R;
import cz.filipobornik.justzoom.base.listeners.OnItemClickListener;
import cz.filipobornik.justzoom.data.model.ItemImageText;
import cz.filipobornik.justzoom.di.PerActivity;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@PerActivity
public class TakenImageGalleryActivityAdapter extends RecyclerView.Adapter<TakenImageGalleryActivityAdapter.ViewHolder>  {

    private List<ItemImageText> items;

    private Context context;

    private OnItemClickListener onClickListener;

    @Inject
    public TakenImageGalleryActivityAdapter(Context context) {
        this.context = context;
        items = new ArrayList<>();
    }

    public void setOnClickListener(OnItemClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public void updateItems(ArrayList<ItemImageText> items) {
        Collections.sort(items);
        Collections.reverse(items);
        this.items = items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ItemImageText item = items.get(position);
        holder.txtTitle.setText(item.getTitle());
        holder.txtDate.setText(item.getCaptureDate());
        Glide.with(context)
                .load(item.getImageUri())
                .thumbnail(0.01f)
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE).skipMemoryCache(true).centerCrop())
                .into(holder.image);
        if (item.isTextSaved()) {
            holder.imgIsTextSaved.setVisibility(View.VISIBLE);
        } else {
            holder.imgIsTextSaved.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img) ImageView image;
        @BindView(R.id.txtTitle) TextView txtTitle;
        @BindView(R.id.txtDate) TextView txtDate;
        @BindView(R.id.imgIsTextSaved) ImageView imgIsTextSaved;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(view -> {
                onClickListener.onItemClick(items.get(getAdapterPosition()));
            });
        }
    }

}
