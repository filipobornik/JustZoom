package cz.filipobornik.justzoom.ui.editImageActivity;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.yalantis.ucrop.UCrop;
import cz.filipobornik.justzoom.R;
import cz.filipobornik.justzoom.base.BaseActivity;
import cz.filipobornik.justzoom.ui.ocrActivity.OCRActivity;
import cz.filipobornik.justzoom.ui.imageDetailActivity.ImageDetailActivity;
import cz.filipobornik.justzoom.utils.BottomNavigationViewHelper;

import javax.inject.Inject;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Edit image
 */
public class EditImageActivity extends BaseActivity implements EditImageActivityContract.View, BottomNavigationView.OnNavigationItemSelectedListener {

    /**
     * Presenter for EditImageActivity
     */
    @Inject EditImageActivityContract.Presenter presenter;

    /**
     * Resource manager that gives access to resources
     */
    @Inject Resources resourceManager;

    @BindView(R.id.bottomNavigation) BottomNavigationView bottomNavigationView;
    @BindView(R.id.imageView) ImageView imageView;

    /**
     * Uri to image that has been taken by camera
     */
    private Uri takenImageUri;

    /**
     * Uri to image that is being currently edited
     */
    private Uri editedImageUri;

    /**
     * Start UCrop activity, create editedImageUri for edited image
     */
    @Override
    public void startUCropActivity() {
        UCrop.Options options = new UCrop.Options();
        options.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        options.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        options.setActiveWidgetColor(ContextCompat.getColor(this, R.color.colorAccent));

        File mediaStorageDir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), resourceManager.getString(R.string.app_name));
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        try {
            File editedImageFile = File.createTempFile("IMG_EDIT_" + timeStamp, ".jpg", mediaStorageDir);
            editedImageUri = Uri.fromFile(editedImageFile);
            UCrop.of(takenImageUri, editedImageUri)
                    .withOptions(options)
                    .start(this);
        } catch (IOException e) {
            e.printStackTrace();
            showError(R.string.error_occurred);
        }
    }

    /**
     * Start OCRActivity that tries to get text from image
     */
    @Override
    public void startOCRActivity() {
        Intent intent = new Intent(this, OCRActivity.class);
        intent.putExtra(OCRActivity.INTENT_EXTRA_IMG_PATH, takenImageUri.getPath());
        startActivity(intent);
    }

    /**
     * Refresh image view that shows preview of image that is being currently edited
     */
    @Override
    public void refreshImageView() {
        RequestOptions requestOptions = RequestOptions
                .skipMemoryCacheOf(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE);

        Glide.with(this)
                .load(editedImageUri)
                .apply(requestOptions)
                .into(imageView);
    }

    /**
     * Save edited images changes, rewrite captured image with edited image
     */
    @Override
    public void saveImageChanges() {
        Log.d("heya", "saved image");
        ContentResolver contentResolver = getBaseContext().getContentResolver();
        FileOutputStream outputStream = null;
        try {
            InputStream inputStream = contentResolver.openInputStream(editedImageUri);
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            outputStream = new FileOutputStream(new File(takenImageUri.getPath()));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            showMessage(R.string.error_occurred);
        }
    }

    /**
     * Delete edited image if the path is not the same as the taken image
     */
    @Override
    public void deleteEditedImage() {
        if (takenImageUri.getPath().equals(editedImageUri.getPath())) {
            return;
        }

        File deleteFile = new File(editedImageUri.getPath());
        if (deleteFile.exists()) {
            deleteFile.delete();
        }
    }

    public void deleteImageTxt() {
        presenter.deleteImageTxt(takenImageUri);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            editedImageUri = UCrop.getOutput(data);
            refreshImageView();
        } else if (resultCode == UCrop.RESULT_ERROR) {
            showError(R.string.error_occurred);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_image);
        presenter.attachView(this);
        ButterKnife.bind(this);
        BottomNavigationViewHelper.removeShiftMode(bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.edit_image);
        }
        if (getIntent().hasExtra(ImageDetailActivity.INTENT_EXTRA_IMG_PATH)) {
            takenImageUri = Uri.fromFile(new File(getIntent().getStringExtra(ImageDetailActivity.INTENT_EXTRA_IMG_PATH)));
            editedImageUri = takenImageUri;
            refreshImageView();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_discard:
                deleteEditedImage();
                deleteImageTxt();
                finish();
                break;
            case R.id.action_edit:
                startUCropActivity();
                break;
            case R.id.action_ocr:
                startOCRActivity();
                break;
            case R.id.action_save:
                presenter.saveImageChanges();
                finish();
                break;
        }

        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }
}
