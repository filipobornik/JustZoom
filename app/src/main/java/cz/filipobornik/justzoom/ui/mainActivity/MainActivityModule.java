package cz.filipobornik.justzoom.ui.mainActivity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import cz.filipobornik.justzoom.base.BaseActivityModule;
import cz.filipobornik.justzoom.di.ActivityContext;
import cz.filipobornik.justzoom.di.PerActivity;
import dagger.Binds;
import dagger.Module;

@Module(includes = BaseActivityModule.class)
public abstract class MainActivityModule {

    @Binds
    @PerActivity
    abstract AppCompatActivity provideActivity(MainActivity cameraActivity);

    @Binds
    @PerActivity
    abstract MainActivityContract.View provideMainActivityAsView(MainActivity cameraActivity);

    @Binds
    @ActivityContext
    @PerActivity
    abstract Context provideMainActivityContext(MainActivity cameraActivity);

    @Binds
    @PerActivity
    abstract MainActivityContract.Presenter providePresenter(MainActivityPresenter presenter);

}
