package cz.filipobornik.justzoom.ui.cameraActivity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import cz.filipobornik.justzoom.base.BaseActivityModule;
import cz.filipobornik.justzoom.di.ActivityContext;
import cz.filipobornik.justzoom.di.PerActivity;
import dagger.Binds;
import dagger.Module;

@Module(includes = BaseActivityModule.class)
public abstract class CameraActivityModule {

    @Binds
    @PerActivity
    abstract AppCompatActivity provideActivity(CameraActivity cameraActivity);

    @Binds
    @PerActivity
    abstract CameraActivityContract.View provideMainActivityAsView(CameraActivity cameraActivity);

    @Binds
    @ActivityContext
    @PerActivity
    abstract Context provideMainActivityContext(CameraActivity cameraActivity);

    @Binds
    @PerActivity
    abstract CameraActivityContract.Presenter providePresenter(CameraActivityPresenter presenter);

}
