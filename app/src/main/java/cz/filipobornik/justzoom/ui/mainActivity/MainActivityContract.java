package cz.filipobornik.justzoom.ui.mainActivity;

import cz.filipobornik.justzoom.base.BaseMVPPresenter;
import cz.filipobornik.justzoom.base.BaseMVPView;

public abstract class MainActivityContract {

    interface View extends BaseMVPView {

        void startCameraActivity();

        void startWalkthroughActivity();

    }

    interface Presenter extends BaseMVPPresenter<View> {

    }

}
