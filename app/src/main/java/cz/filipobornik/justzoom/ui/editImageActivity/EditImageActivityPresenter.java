package cz.filipobornik.justzoom.ui.editImageActivity;

import android.net.Uri;
import cz.filipobornik.justzoom.base.BasePresenter;
import cz.filipobornik.justzoom.data.DataManager;
import cz.filipobornik.justzoom.di.PerActivity;

import javax.inject.Inject;

@PerActivity
public class EditImageActivityPresenter extends BasePresenter<EditImageActivityContract.View>
        implements EditImageActivityContract.Presenter {

    private DataManager dataManager;

    @Inject
    public EditImageActivityPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    /**
     * Save image changes and deleted temp edited image
     */
    @Override
    public void saveImageChanges() {
        if (isViewAttached()) {
            view.saveImageChanges();
            view.deleteEditedImage();
        }
    }

    @Override
    public void deleteImageTxt(Uri takenImageUri) {
        dataManager.deleteTxtFileForImage(takenImageUri);
    }
}
