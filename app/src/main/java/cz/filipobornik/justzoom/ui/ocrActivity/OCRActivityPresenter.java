package cz.filipobornik.justzoom.ui.ocrActivity;

import android.net.Uri;
import cz.filipobornik.justzoom.R;
import cz.filipobornik.justzoom.base.BasePresenter;
import cz.filipobornik.justzoom.data.DataManager;
import cz.filipobornik.justzoom.di.PerActivity;

import javax.inject.Inject;
import java.io.File;

@PerActivity
public class OCRActivityPresenter extends BasePresenter<OCRActivityContract.View>
        implements OCRActivityContract.Presenter {

    /**
     * DataManager for saving and downloading data
     */
    private DataManager dataManager;

    @Inject
    public OCRActivityPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void getTextFromImage(Uri takenImageUri) {
        view.showLoading();
        compositeDisposable.add(dataManager.getTextFromImage(new File(takenImageUri.getPath()))
                .subscribe(text -> {
                            view.hideLoading();
                            if (isViewAttached()) {
                                view.showTextFromImage(text);
                            }
                        },
                        error -> {
                            view.hideLoading();
                            view.showError(error.getLocalizedMessage());
                        }));
    }

    @Override
    public void onTextToSpeechBtnClick(String text) {
        if(isViewAttached()) {
            view.textToSpeech(text);
        }
    }

    @Override
    public void onSaveTextBtnClicked(Uri imageUri, String text) {
        Uri txtUri = dataManager.saveTextAssociatedWithImage(imageUri, text);
        if (isViewAttached()) {
            if (txtUri != null) {
                view.showMessage(R.string.txt_saved);
                view.closeActivity();
            } else {
                view.showError(R.string.txt_not_saved);
            }
        }
    }
}
