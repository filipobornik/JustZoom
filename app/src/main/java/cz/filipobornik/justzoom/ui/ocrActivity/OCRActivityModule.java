package cz.filipobornik.justzoom.ui.ocrActivity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import cz.filipobornik.justzoom.base.BaseActivityModule;
import cz.filipobornik.justzoom.di.ActivityContext;
import cz.filipobornik.justzoom.di.PerActivity;
import dagger.Binds;
import dagger.Module;

@Module(includes = BaseActivityModule.class)
public abstract class OCRActivityModule {

    @Binds
    @PerActivity
    abstract AppCompatActivity provideActivity(OCRActivity cameraActivity);

    @Binds
    @PerActivity
    abstract OCRActivityContract.View provideMainActivityAsView(OCRActivity cameraActivity);

    @Binds
    @ActivityContext
    @PerActivity
    abstract Context provideMainActivityContext(OCRActivity cameraActivity);

    @Binds
    @PerActivity
    abstract OCRActivityContract.Presenter providePresenter(OCRActivityPresenter presenter);

}
