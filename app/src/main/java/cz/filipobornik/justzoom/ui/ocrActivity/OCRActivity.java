package cz.filipobornik.justzoom.ui.ocrActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import cz.filipobornik.justzoom.R;
import cz.filipobornik.justzoom.base.BaseActivity;
import cz.filipobornik.justzoom.ui.imageDetailActivity.ImageDetailActivity;
import cz.filipobornik.justzoom.utils.BottomNavigationViewHelper;

import javax.inject.Inject;
import java.io.File;
import java.util.Locale;

public class OCRActivity extends BaseActivity implements OCRActivityContract.View, BottomNavigationView.OnNavigationItemSelectedListener, TextToSpeech.OnInitListener {

    private static final int ACT_CHECK_TTS_DATA = 102;
    /**
     * Intent extra image path key
     */
    public static String INTENT_EXTRA_IMG_PATH = "ImageDetailActivity.intentExtraImagePath";

    /**
     * Presenter for OCRActivity
     */
    @Inject OCRActivityContract.Presenter presenter;

    /**
     * Shared preferences provide user's settings like language, color theme, etc
     */
    @Inject SharedPreferences sharedPreferences;

    @BindView(R.id.textFromImage) TextView txtFromImage;
    @BindView(R.id.loadingBar) LinearLayout loadingBar;
    @BindView(R.id.bottomNavigation) BottomNavigationView bottomNavigation;

    /**
     * Image taken Uri
     */
    private Uri takenImageUri;

    private TextToSpeech textToSpeech;

    /**
     * Set text from image into text view
     *
     * @param text text from image
     */
    @Override
    public void showTextFromImage(String text) {
        if (text.isEmpty()) {
            txtFromImage.setText(R.string.ups_text_not_found);
            disableBottomNavActions();
        } else {
            txtFromImage.setText(text);
            enableBottomNavActions();
        }
    }

    private void enableBottomNavActions() {
        bottomNavigation.getMenu().getItem(1).setEnabled(true);
        bottomNavigation.getMenu().getItem(2).setEnabled(true);
        bottomNavigation.getMenu().getItem(3).setEnabled(true);
    }

    private void disableBottomNavActions() {
        bottomNavigation.getMenu().getItem(1).setEnabled(false);
        bottomNavigation.getMenu().getItem(2).setEnabled(false);
        bottomNavigation.getMenu().getItem(3).setEnabled(false);
    }

    /**
     * Show loading bar
     */
    @Override
    public void showLoading() {
        loadingBar.setVisibility(View.VISIBLE);
    }

    /**
     * Hide loading bar
     */
    @Override
    public void hideLoading() {
        loadingBar.setVisibility(View.GONE);
    }

    /**
     * Let user share text from image
     */
    @Override
    public void shareText() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Text from image: " + takenImageUri.getEncodedPath() +
                "\n" + txtFromImage.getText());
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
    }

    /**
     * Read text from image
     *
     * @param text text to read
     */
    @Override
    public void textToSpeech(String text) {
        textToSpeech.speak(txtFromImage.getText(), TextToSpeech.QUEUE_FLUSH, null, null);
    }

    /**
     * Start OCR animation
     */
    private void startAnimation() {
        final View bar = findViewById(R.id.bar);
        final Animation animation = AnimationUtils.loadAnimation(this, R.anim.scan_animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                bar.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        bar.setVisibility(View.VISIBLE);
        bar.startAnimation(animation);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ACT_CHECK_TTS_DATA) {
            if (resultCode ==
                    TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                // Data exists, so we instantiate the TTS engine
                textToSpeech = new TextToSpeech(getApplicationContext(), this);
            } else {
                // Data is missing, so we start the TTS installation
                // process
                Intent installIntent = new Intent();
                installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installIntent);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ocr);
        ButterKnife.bind(this);
        presenter.attachView(this);
        BottomNavigationViewHelper.removeShiftMode(bottomNavigation);
        bottomNavigation.setOnNavigationItemSelectedListener(this);
        startAnimation();
        Intent ttsIntent = new Intent();
        ttsIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(ttsIntent, ACT_CHECK_TTS_DATA);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.ocr);
        }

        Log.d("txtToSpeech", "should speak");
        if (getIntent().hasExtra(ImageDetailActivity.INTENT_EXTRA_IMG_PATH)) {
            takenImageUri = Uri.fromFile(new File(getIntent().getStringExtra(ImageDetailActivity.INTENT_EXTRA_IMG_PATH)));
            presenter.getTextFromImage(takenImageUri);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_discard:
                finish();
                break;
            case R.id.action_read:
                presenter.onTextToSpeechBtnClick(txtFromImage.getText().toString());
                break;
            case R.id.action_share:
                shareText();
                break;
            case R.id.action_save:
                presenter.onSaveTextBtnClicked(takenImageUri, txtFromImage.getText().toString());
                break;
        }

        return true;
    }

    /**
     * Finishes activity
     */
    @Override
    public void closeActivity() {
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (textToSpeech != null) {
            textToSpeech.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            if (textToSpeech != null) {
                String language = sharedPreferences.getString("pref_speak_language", "en");
                int result = textToSpeech.setLanguage(new Locale(language));
                if (result == TextToSpeech.LANG_MISSING_DATA || result ==
                        TextToSpeech.LANG_NOT_SUPPORTED) {
                    Toast.makeText(this, "TTS language is not supported",
                            Toast.LENGTH_LONG).show();
                } else {
                    // Do something here
                }
            }
        } else {
            Toast.makeText(this, "TTS initialization failed",
                    Toast.LENGTH_LONG).show();
        }
    }
}
