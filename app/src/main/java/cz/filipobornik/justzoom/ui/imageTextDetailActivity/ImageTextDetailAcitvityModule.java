package cz.filipobornik.justzoom.ui.imageTextDetailActivity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import cz.filipobornik.justzoom.base.BaseActivityModule;
import cz.filipobornik.justzoom.di.ActivityContext;
import cz.filipobornik.justzoom.di.PerActivity;
import dagger.Binds;
import dagger.Module;

@Module(includes = BaseActivityModule.class)
public abstract class ImageTextDetailAcitvityModule {

    @Binds
    @PerActivity
    abstract AppCompatActivity provideActivity(ImageTextDetailActivity activity);

    @Binds
    @PerActivity
    abstract ImageTextDetailActivityContract.View provideMainActivityAsView(ImageTextDetailActivity activity);

    @Binds
    @ActivityContext
    @PerActivity
    abstract Context provideMainActivityContext(ImageTextDetailActivity activity);

    @Binds
    @PerActivity
    abstract ImageTextDetailActivityContract.Presenter providePresenter(ImageTextDetailActivityPresenter presenter);

}
