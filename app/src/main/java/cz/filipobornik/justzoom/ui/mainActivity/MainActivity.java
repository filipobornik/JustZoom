package cz.filipobornik.justzoom.ui.mainActivity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import cz.filipobornik.justzoom.R;
import cz.filipobornik.justzoom.base.BaseActivity;
import cz.filipobornik.justzoom.ui.cameraActivity.CameraActivity;
import cz.filipobornik.justzoom.utils.Constants;
import cz.filipobornik.justzoom.walkthrough.WalkthroughActivity;

import javax.inject.Inject;

/**
 * MainActivity
 */
public class MainActivity extends BaseActivity implements MainActivityContract.View {

    /**
     * Presenter for MainActivity
     */
    @Inject MainActivityContract.Presenter presenter;

    /**
     * Start CameraActivity
     */
    @Override
    public void startCameraActivity() {
        Intent intent = new Intent(this, CameraActivity.class);
        startActivity(intent);
    }

    @Override
    public void startWalkthroughActivity() {
        Intent intent = new Intent(this, WalkthroughActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(Constants.PREF_FIRST_START, true)) {
            startWalkthroughActivity();
            finish();
        } else {
            presenter.attachView(this);
            startCameraActivity();
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }
}
