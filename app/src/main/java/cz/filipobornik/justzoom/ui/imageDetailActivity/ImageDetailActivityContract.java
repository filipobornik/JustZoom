package cz.filipobornik.justzoom.ui.imageDetailActivity;

import android.net.Uri;
import cz.filipobornik.justzoom.base.BaseMVPPresenter;
import cz.filipobornik.justzoom.base.BaseMVPView;

public abstract class ImageDetailActivityContract {

    interface View extends BaseMVPView {

        void startCameraActivity();

        void startEditImageActivity();

        void deleteImage();

    }

    interface Presenter extends BaseMVPPresenter<View> {

        void deleteImageTxt(Uri takenImageUri);

    }
}
