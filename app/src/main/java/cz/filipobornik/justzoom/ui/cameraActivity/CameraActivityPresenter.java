package cz.filipobornik.justzoom.ui.cameraActivity;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import com.wonderkiln.camerakit.CameraKitImage;
import cz.filipobornik.justzoom.R;
import cz.filipobornik.justzoom.base.BasePresenter;
import cz.filipobornik.justzoom.data.DataManager;
import cz.filipobornik.justzoom.di.PerActivity;

import javax.inject.Inject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Presenter for CameraActivity
 */
@PerActivity
public class CameraActivityPresenter extends BasePresenter<CameraActivityContract.View>
        implements CameraActivityContract.Presenter {

    /**
     * Indicates if flash is enabled or not
     */
    private boolean flashEnabled = false;

    /**
     * Provide access to app's resources
     */
    private DataManager dataManager;


    @Inject
    public CameraActivityPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    /**
     * Handle FAB Capture clicked
     *
     * @param cameraKitImage captured image
     */
    @Override
    public void onFabCaptureClicked(CameraKitImage cameraKitImage) {
        if (isViewAttached()) {
            Bitmap bitmap = cameraKitImage.getBitmap();
            Uri imageUri = dataManager.saveImageIntoStorage(bitmap);
            view.hideLoading();
            view.startDetailImageActivity(imageUri);
        }
    }

    /**
     * Handle FAB Flash click, notifies view to enable or disable flash depending on previous state
     */
    @Override
    public void onFabFlashClicked() {
        flashEnabled = !flashEnabled;
        if (flashEnabled) {
            if (isViewAttached()) view.turnFlashOn();
        } else {
            if (isViewAttached()) view.turnFlashOff();
        }
    }

    /**
     * Handle btn settings click, notify view to open Settings Activity
     */
    @Override
    public void onBtnSettingsClicked() {
        if (isViewAttached()) {
            view.startSettingsActivity();
        }
    }

    /**
     * Handle btn saved image gallery click, notify view to open Settings Activity
     */
    @Override
    public void onBtnSavedImagesGallery() {
        if (isViewAttached()) {
            view.startPickImageActivity();
        }
    }

    /**
     * Handle btn gallery click
     */
    @Override
    public void onFabGalleryClicked() {
        if (isViewAttached()) {
            view.startTakenImagesGalleryActivity();
        }
    }

    @Override
    public void onImgSelected(Bitmap bitmap) {
        Uri imageUri = dataManager.saveImageIntoStorage(bitmap);
        view.startDetailImageActivity(imageUri);
    }
}
