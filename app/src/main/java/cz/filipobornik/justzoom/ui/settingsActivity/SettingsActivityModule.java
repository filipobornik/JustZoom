package cz.filipobornik.justzoom.ui.settingsActivity;

import android.content.Context;
import cz.filipobornik.justzoom.di.ActivityContext;
import cz.filipobornik.justzoom.di.PerActivity;
import dagger.Binds;
import dagger.Module;

@Module
public abstract class SettingsActivityModule {

    @Binds
    @ActivityContext
    @PerActivity
    abstract Context provideMainActivityContext(SettingsActivity cameraActivity);

}
