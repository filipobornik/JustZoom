package cz.filipobornik.justzoom.ui.ocrActivity;

import android.net.Uri;
import cz.filipobornik.justzoom.base.BaseMVPPresenter;
import cz.filipobornik.justzoom.base.BaseMVPView;

public abstract class OCRActivityContract {

    interface View extends BaseMVPView {

        void showTextFromImage(String text);

        void showLoading();

        void hideLoading();

        void shareText();

        void textToSpeech(String text);

        void closeActivity();

    }

    interface Presenter extends BaseMVPPresenter<View> {

        void getTextFromImage(Uri takenImageUri);

        void onTextToSpeechBtnClick(String text);

        void onSaveTextBtnClicked(Uri takenImageUri, String text);

    }

}
