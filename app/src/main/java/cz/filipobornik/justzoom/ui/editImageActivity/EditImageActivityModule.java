package cz.filipobornik.justzoom.ui.editImageActivity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import cz.filipobornik.justzoom.base.BaseActivityModule;
import cz.filipobornik.justzoom.di.ActivityContext;
import cz.filipobornik.justzoom.di.PerActivity;
import dagger.Binds;
import dagger.Module;

@Module(includes = BaseActivityModule.class)
public abstract class EditImageActivityModule {

    @Binds
    @PerActivity
    abstract AppCompatActivity provideActivity(EditImageActivity cameraActivity);

    @Binds
    @PerActivity
    abstract EditImageActivityContract.View provideMainActivityAsView(EditImageActivity cameraActivity);

    @Binds
    @ActivityContext
    @PerActivity
    abstract Context provideMainActivityContext(EditImageActivity cameraActivity);

    @Binds
    @PerActivity
    abstract EditImageActivityContract.Presenter providePresenter(EditImageActivityPresenter presenter);

}
