package cz.filipobornik.justzoom.ui.imageDetailActivity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import cz.filipobornik.justzoom.base.BaseActivityModule;
import cz.filipobornik.justzoom.di.ActivityContext;
import cz.filipobornik.justzoom.di.PerActivity;
import dagger.Binds;
import dagger.Module;

@Module(includes = BaseActivityModule.class)
public abstract class ImageDetailActivityModule {

    @Binds
    @PerActivity
    abstract AppCompatActivity provideActivity(ImageDetailActivity cameraActivity);

    @Binds
    @PerActivity
    abstract ImageDetailActivityContract.View provideMainActivityAsView(ImageDetailActivity cameraActivity);

    @Binds
    @ActivityContext
    @PerActivity
    abstract Context provideMainActivityContext(ImageDetailActivity cameraActivity);

    @Binds
    @PerActivity
    abstract ImageDetailActivityContract.Presenter providePresenter(ImageDetailActivityPresenter presenter);

}
