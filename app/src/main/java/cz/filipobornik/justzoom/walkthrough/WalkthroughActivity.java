package cz.filipobornik.justzoom.walkthrough;

import android.content.Intent;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.ramotion.paperonboarding.PaperOnboardingFragment;
import com.ramotion.paperonboarding.PaperOnboardingPage;
import cz.filipobornik.justzoom.R;
import cz.filipobornik.justzoom.ui.mainActivity.MainActivity;
import cz.filipobornik.justzoom.utils.Constants;

import java.util.ArrayList;

public class WalkthroughActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walkthrough);

        PaperOnboardingPage scr1 = new PaperOnboardingPage(getString(R.string.zoom_title),
                getString(R.string.zoom_text),
                Color.parseColor("#FFFFFF"), R.drawable.ic_zoom, R.drawable.ic_zoom_small);
        PaperOnboardingPage scr2 = new PaperOnboardingPage(getString(R.string.scan_title),
                getString(R.string.scan_text),
                Color.parseColor("#FFFFFF"), R.drawable.ic_scan_text, R.drawable.ic_scan_small);
        PaperOnboardingPage scr3 = new PaperOnboardingPage(getString(R.string.listen_title),
                getString(R.string.listen_text),
                Color.parseColor("#FFFFFF"), R.drawable.ic_read, R.drawable.ic_read_small);

        ArrayList<PaperOnboardingPage> elements = new ArrayList<>();
        elements.add(scr1);
        elements.add(scr2);
        elements.add(scr3);

        PaperOnboardingFragment onBoardingFragment = PaperOnboardingFragment.newInstance(elements);

        getSupportFragmentManager().beginTransaction()
        .add(R.id.container, onBoardingFragment)
        .commit();

        onBoardingFragment.setOnRightOutListener(() -> {
            PreferenceManager.getDefaultSharedPreferences(WalkthroughActivity.this).edit().putBoolean(Constants.PREF_FIRST_START, false).apply();
            Intent intent = new Intent(WalkthroughActivity.this, MainActivity.class);
            startActivity(intent);
        });
    }
}
