package cz.filipobornik.justzoom.base;

import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;

public interface BaseMVPView {

    void showMessage(String message);

    void showMessage(@StringRes int messageResourceId);

    void showError(String error);

    void showError(@StringRes int messageResourceId);

    void addFragment(@IdRes int id, Fragment fragment, String toolbarTitle);

}
