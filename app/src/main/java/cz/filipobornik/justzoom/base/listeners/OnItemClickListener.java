package cz.filipobornik.justzoom.base.listeners;

import cz.filipobornik.justzoom.data.model.ItemImageText;

public interface OnItemClickListener {

    public void onItemClick(ItemImageText itemImageText);

}
