package cz.filipobornik.justzoom.base;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Base Presenter class every presenter has to extend from
 */
public class BasePresenter<V extends BaseMVPView> implements BaseMVPPresenter<V> {

    /**
     * View attached to the presenter
     */
    protected V view;

    /**
     * For disposing all observes when view is detached
     */
    protected CompositeDisposable compositeDisposable = new CompositeDisposable();

    /**
     * Attach view to presenter
     */
    @Override
    public void attachView(V view) {
        this.view = view;
    }

    /**
     * Detach view from presenter, dispose composite disposable
     */
    @Override
    public void detachView() {
        this.view = null;
        this.compositeDisposable.dispose();
    }

    /**
     * Check if view is attached to the presenter
     */
    @Override
    public boolean isViewAttached() {
        return view != null;
    }
}
