package cz.filipobornik.justzoom.base;

public interface BaseMVPPresenter<V extends BaseMVPView> {

    void attachView(V view);

    void detachView();

    boolean isViewAttached();

}
