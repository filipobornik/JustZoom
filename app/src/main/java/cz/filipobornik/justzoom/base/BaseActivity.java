package cz.filipobornik.justzoom.base;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import butterknife.ButterKnife;
import com.google.firebase.analytics.FirebaseAnalytics;
import cz.filipobornik.justzoom.R;
import cz.filipobornik.justzoom.utils.AppColorThemeManager;
import dagger.android.AndroidInjection;

import javax.inject.Inject;

/**
 * BaseActivity that every activity has to inherit from
 * Provides basic functionality for activity
 */
public class BaseActivity extends AppCompatActivity implements BaseMVPView {

    /**
     * Resource manager for accessing resources
     */
    @Inject Resources resourceManager;

    /**
     * Fragment manager for inserting fragments into Activity
     */
    @Inject protected FragmentManager fragmentManager;

    /**
     * Firebase analytics to log events
     */
    @Inject FirebaseAnalytics firebaseAnalytics;

    /**
     * ColorThemeManager handle color themes
     */
    @Inject AppColorThemeManager colorThemeManager;

    /**
     * Shows custom message
     */
    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Shows message from string resources
     */
    @Override
    public void showMessage(int messageResourceId) {
        Toast.makeText(this, resourceManager.getString(messageResourceId), Toast.LENGTH_SHORT).show();
    }

    /**
     * Shows custom error message when error appears with ok button
     */
    @Override
    public void showError(String errorMessage) {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), errorMessage, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.ok, v -> snackbar.dismiss());
        snackbar.show();
    }

    /**
     * Shows error message from string resources when error appears with ok button
     */
    @Override
    public void showError(int messageResourceId) {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), resourceManager.getText(messageResourceId), Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.ok, v -> snackbar.dismiss());
        snackbar.show();
    }

    /**
     * Adds fragment into Activity, set title to toolbar
     */
    @Override
    public void addFragment(@IdRes int id, Fragment fragment, String toolbarTitle) {
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(id, fragment)
                .disallowAddToBackStack()
                .commit();

        getSupportActionBar().setTitle(toolbarTitle);
    }

    /**
     * Inject activity in Dagger dependency graph
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        setTheme(colorThemeManager.getAppColorTheme());
        super.onCreate(savedInstanceState);
    }
}
