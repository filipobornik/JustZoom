package cz.filipobornik.justzoom.di.component;

import cz.filipobornik.justzoom.JustZoomApp;
import cz.filipobornik.justzoom.data.remote.NetModule;
import cz.filipobornik.justzoom.di.module.AppModule;
import dagger.Component;
import dagger.android.AndroidInjector;

import javax.inject.Singleton;

@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface AppComponent extends AndroidInjector<JustZoomApp> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<JustZoomApp> {}

}
