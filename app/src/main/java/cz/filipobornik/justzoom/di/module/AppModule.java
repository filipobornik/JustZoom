package cz.filipobornik.justzoom.di.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import com.google.firebase.analytics.FirebaseAnalytics;
import cz.filipobornik.justzoom.JustZoomApp;
import cz.filipobornik.justzoom.R;
import cz.filipobornik.justzoom.ui.imageTextDetailActivity.ImageTextDetailAcitvityModule;
import cz.filipobornik.justzoom.ui.imageTextDetailActivity.ImageTextDetailActivity;
import cz.filipobornik.justzoom.ui.ocrActivity.OCRActivity;
import cz.filipobornik.justzoom.ui.ocrActivity.OCRActivityModule;
import cz.filipobornik.justzoom.ui.cameraActivity.CameraActivity;
import cz.filipobornik.justzoom.di.ApplicationContext;
import cz.filipobornik.justzoom.di.PerActivity;
import cz.filipobornik.justzoom.ui.cameraActivity.CameraActivityModule;
import cz.filipobornik.justzoom.ui.editImageActivity.EditImageActivity;
import cz.filipobornik.justzoom.ui.editImageActivity.EditImageActivityModule;
import cz.filipobornik.justzoom.ui.imageDetailActivity.ImageDetailActivity;
import cz.filipobornik.justzoom.ui.imageDetailActivity.ImageDetailActivityModule;
import cz.filipobornik.justzoom.ui.mainActivity.MainActivity;
import cz.filipobornik.justzoom.ui.mainActivity.MainActivityModule;
import cz.filipobornik.justzoom.ui.settingsActivity.SettingsActivityModule;
import cz.filipobornik.justzoom.ui.settingsActivity.SettingsActivity;
import cz.filipobornik.justzoom.ui.takenImagesGalleryActivity.TakenImagesGalleryActivity;
import cz.filipobornik.justzoom.ui.takenImagesGalleryActivity.TakenImagesGalleryActivityModule;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.AndroidInjectionModule;
import dagger.android.ContributesAndroidInjector;
import javax.inject.Singleton;

@Module(includes = {AndroidInjectionModule.class})
public abstract class AppModule {

    @Binds
    @ApplicationContext
    @Singleton
    abstract Context provideContext(JustZoomApp app);

    @Binds
    @Singleton
    abstract Application provideApplication(JustZoomApp app);

    @Provides
    @Singleton
    static Resources provideResourceManager(JustZoomApp app) {
        return app.getResources();
    }

    @Provides
    @Singleton
    static SharedPreferences provideSharedPreferences(JustZoomApp app, Resources resourceManager) {
        return app.getSharedPreferences(resourceManager.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    static FirebaseAnalytics provideFirebaseAnalytics(@ApplicationContext Context context) {
        return FirebaseAnalytics.getInstance(context);
    }

    @PerActivity
    @ContributesAndroidInjector(modules = {CameraActivityModule.class})
    abstract CameraActivity cameraActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {SettingsActivityModule.class})
    abstract SettingsActivity settingsActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {MainActivityModule.class})
    abstract MainActivity mainActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {ImageDetailActivityModule.class})
    abstract ImageDetailActivity imageDetailActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {EditImageActivityModule.class})
    abstract EditImageActivity editImageActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {OCRActivityModule.class})
    abstract OCRActivity ocrActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {TakenImagesGalleryActivityModule.class})
    abstract TakenImagesGalleryActivity takenImagesGalleryActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {ImageTextDetailAcitvityModule.class})
    abstract ImageTextDetailActivity imageTextDetailActivity();

}
